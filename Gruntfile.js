module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      files: {
        src: ['./src/**/*.js']
      }
    },
    mochaTest: {
      test: {
        options: {
          globals: 'chai',
          timeout: 3000,
          ignoreLeaks: false,
          ui: 'bdd',
          reporter: 'list'
        },
        src: ['test/**/*.js']
      }
    },
    browserify: {
      all: {
        files: {
          'onion.js': './index.js'
        }, 
        options: {
          alias: ['./index.js:onion']
        }
      } 
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> - <%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        report: 'gzip'
      },
      all: {
        files: {
          './onion.min.js': ['./onion.js']
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-mocha-test');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.registerTask('test', [
    'jshint',
    'mochaTest'
  ]);
  grunt.registerTask('build', [
    'test',
    'browserify',
    'uglify'
  ]);
};
