var _ = require('lodash');
var async = require('async');

function Onion(layers) { 
  this.layers = layers && layers.length ? layers : []; 
}

Onion.prototype = {

  layer: function (c) {
    this.layers.push(c);
    return this;
  },

  compile: function (cb) {
    var self = this;
    this.config = {};
    async.reduce(this.layers, this.config, 
      function(memo, item, done){
        item = typeof item === 'function' ? item() : item;
        return typeof item === 'string' ?
          self.parse(item, function (err, item) {
            done(err, _.merge(memo, item)); 
          }) : done(null, _.merge(memo, item)); 
    }, function (err, results) {
      self.config = results;
      self.config = self.process.call(self, results);
      cb(err, self.config);
    });
  },

  parse: function (string, done) {
    var parts = string.split('!');
    var plugin = Onion.plugins[parts[0]];
    var arg = this.interpolate(parts[1]);
    return plugin ? plugin.call(this, arg, done) : done();
  },

  process: function (config, key, root) {
    if (typeof config === 'object') {
      if (config.$ref) {
        root[key] = config = this.reference(config.$ref);
      }
      if(config.$extend) {
        var ref = config.$extend;
        delete config.$extend;
        root[key] = config = this.extend(ref, config);
      }
      return _.each(config, this.process, this);
    }
    if (typeof config === 'string') {
      return root[key] = this.interpolate(config, key, root);
    }
  },

  extend: function (ref, ext) {
    return _.merge(_.reduce(ref.split('.'), 
      function (i, j) { return i ? i[j] : null;
    }, this.config), ext);
  },

  reference: function (ref) {
    return _.cloneDeep(_.reduce(ref.split('.'), 
      function (i, j) { return i ? i[j] : null;
    }, this.config));
  },

  interpolate: function (string) {
    return _.template(string)({ config: this.config });
  }

};

Onion.plugins = {};
Onion.plugins.file = require('./src/plugins/file');
Onion.plugins.argv = require('./src/plugins/argv');
Onion.plugins.env = require('./src/plugins/env');
Onion.plugins.url = require('./src/plugins/url');

module.exports = Onion;