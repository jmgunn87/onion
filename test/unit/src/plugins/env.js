var env = require('./../../../../src/plugins/env');
var assert = require('assert');

describe("\"env!\" plugin", function () {
  it("loads in configurations from environment variables under the given prefix", function (done) {
    process.env.TEST_ONION_NUMBER = 1;
    process.env.TEST_ONION_STRING = "hello world";
    env("TEST_ONION_", function (err, config) {
      assert.equal(config.number, 1);
      assert.equal(config.string, "hello world");
      done();
    }); 
  });
});