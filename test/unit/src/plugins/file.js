var file = require('./../../../../src/plugins/file');
var assert = require('assert');

describe("\"file!\" plugin", function () {

  it("throws an exception when its argument is a location to a file that doesn't exist", function (done) {
    file("nowhere", function (err) { 
      assert.ok(err);
      done();
    });
  });
  it("returns a json decoded config object from a file", function (done) {
    file(__dirname + "/../../../fixtures/config.json", function (err, data) {
      if (err) throw err;
      assert.equal(data.name, "config");
      done();
    });
  });
  it("accepts js or json file formats", function (done) {
    file(__dirname + "/../../../fixtures/config.js", function (err, data) {
      if (err) throw err;
      assert.equal(data.name, "config");
      done();
    });
  });

});