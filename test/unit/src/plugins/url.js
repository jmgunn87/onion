var url = require('./../../../../src/plugins/url');
var assert = require('assert');
var http = require('http');

describe("\"url!\" plugin", function () {
  
  before(function (done) {
    this.server = http.createServer(function (req, res) {
      res.end(JSON.stringify({ name: "hello world" }));
    }).listen(9329, done);
  });
  
  after(function () {
    this.server.close();
  });

  it("loads in configurations from the given url", function (done) {
    url("http://localhost:9329", function (err, config) {
      if (err) throw err;
      assert.equal(config.name, "hello world");
      done();
    }); 
  });
});