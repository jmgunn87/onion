var argv = require('./../../../../src/plugins/argv');
var assert = require('assert');

describe("\"argv!\" plugin", function () {
  it("loads in configurations from command line arguments", function (done) {
    process.argv.push("--number");
    process.argv.push("1");
    process.argv.push("--string");
    process.argv.push("hello world");
    argv("TEST_ONION_", function (err, config) {
      assert.equal(config.number, 1);
      assert.equal(config.string, "hello world");
      done();
    }); 
  });
});