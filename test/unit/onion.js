var Onion = require('./../../index');
var assert = require('assert');

describe("Onion", function () {
  
  beforeEach(function () {
    this.instance = new Onion();
  });

  describe("#constructor", function () {
    it("allows an array of layers to be passed to it", function () {
      var onion = new Onion([{}, {}, {}]);
      assert.equal(onion.layers.length, 3);
    });
  });
  describe("#layer", function () {
    it("adds a layer of configuration", function () {
      this.instance.layer({});
      this.instance.layer({});
      this.instance.layer({});
      assert.equal(this.instance.layers.length, 3);
    });
  });

  describe("#compile", function () {
    it("merges all layers of configuration and returns the result asynchronously", function (done) {
      this.instance.layer({ a: 1, b:1, c:1 });
      this.instance.layer({ a: 2, b:2 });
      this.instance.layer({ a: 3 });
      this.instance.compile(function (err, data) {
        if (err) throw err;
        assert.equal(data.a, 3);
        assert.equal(data.b, 2);
        assert.equal(data.c, 1);
        done();
      });
    });
    it("deep merging is perfomed through all resulting objects", function (done) {
      this.instance.layer({ a: { b:1, c:1 } });
      this.instance.layer({ a: { b:2 } });
      this.instance.layer({ a: {} });
      this.instance.compile(function (err, data) {
        if (err) throw err;
        assert.equal(data.a.b, 2);
        assert.equal(data.a.c, 1);
        done();
      });
    });
    it("executes any layers that are functions", function (done) {
      this.instance.layer(function () { return { a: 1 }; });
      this.instance.compile(function (err, data) {
        if (err) throw err;
        assert.equal(data.a, 1);
        done();
      });
    });
    it("performs interpolation on all strings using templating parameters", function (done) {
      this.instance.layer({ nest: { message: "<%= config.string %> world" } });
      this.instance.layer({ string: "hello" });
      this.instance.compile(function (err, data) {
        if (err) throw err;
        assert.equal(data.nest.message, "hello world");
        done();
      });
    });
    it("accepts the $ref(http://tools.ietf.org/html/draft-pbryan-zyp-json-ref-03) specification", function (done) {
      this.instance.layer({ message: "hello world" });
      this.instance.layer({ nested: { string: { $ref: "message" } } });
      this.instance.compile(function (err, data) {
        if (err) throw err;
        assert.equal(data.nested.string, "hello world");
        done();
      });
    });
    it("parses an $extend key performing an extension on its reference", function (done) {
      this.instance.layer({ message: {
        part1: "hello",
        part2: "england" } });
      this.instance.layer({ nested: { string: { 
        $extend: "message",
        part2: "world",
        part3: "!" } } });
      this.instance.compile(function (err, data) {
        if (err) throw err;
        assert.equal(data.nested.string.part1, "hello");
        assert.equal(data.nested.string.part2, "world");
        assert.equal(data.nested.string.part3, "!");
        done();
      });
    });
    it("recognizes plugins using the plugin syntax", function (done) {
      Onion.plugins.test = function (location, done) { done(null, { name: location }); };
      this.instance.layer("test!config");
      this.instance.compile(function (err, data) {
        if (err) throw err;
        assert.equal(data.name, "config");
        done();
      });
    });
    it("interpolates plugin arguments", function (done) {
      Onion.plugins.test = function (location, done) { 
        assert.equal(location, "hello world");
        done(null, { name: location }); 
      };
      this.instance.layer({ string: "hello" });
      this.instance.layer("test!<%= config.string %> world");
      this.instance.compile(function (err, data) {
        if (err) throw err;
        assert.equal(data.name, "hello world");
        done();
      });
    });
    it("doesnt recognize any interpolation variables for plugin arguments if they have not been defined", function (done) {
      Onion.plugins.test = function (location, done) { 
        assert.notEqual(location, "hello world");
        done(null, { name: location }); 
      };
      this.instance.layer("test!<%= config.string %> world");
      this.instance.compile(function (err, data) {
        if (err) throw err;
        done();
      });
    });
  });

});