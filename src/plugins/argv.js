module.exports = function (prefix, done) {
  var argv = require('optimist').argv;
  delete argv.$0;
  delete argv._;
  done(null, argv);
};