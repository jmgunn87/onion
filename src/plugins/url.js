var request = require('request');

module.exports = function (url, done) {
  request.get(url, function (err, res, body) {
    done(err, JSON.parse(body));
  });
};