module.exports = function (prefix, done) {
  var config = {};
  for (var key in process.env) {
    if (key.indexOf(prefix) === 0) {
      config[key.replace(prefix, "").toLowerCase()] = process.env[key];
    }
  }
  done(null, config);
};