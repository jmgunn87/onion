var path = require('path');
var fs = require('fs');

module.exports = function (location, done) {
  fs.exists(location, function (exists) {
    if (!exists) return done(new Error('file does not exist at path ' + location));
    if (path.extname(location) === '.js') return done(null, require(location));
    fs.readFile(location, function (err, data) {
      done(null, JSON.parse(data));
    });
  });
};